resource "aws_instance" "WebServer" {
    ami = "ami-0sdfdjn"
    instance_type = "t3.micro"
    vpc_security_group_ids = [aws_security_group.allow_tls] 
    user_data = file("file.sh")
#if add lificycle this type, instance no delete.
lificycle {
    prevent_destroy = true
}
#if add this lifecycle, in the run file.tf was ignored listed directives 
lificycle {
    ignore_changes = ["ami", "user_data"]
}
#create new server before drop a existing 
lificycle {
    create_before_destroy = true
}

}
  
#add a eip for to instance 
resource "aws_eip" "my_static_ip" {
    instance = "aws_instance.my_webserver"
}
resource "aws_instance" "my_webserver" {
  ami = "ami-8298u9uhifh"
}
